# Oil Tanker Unloading Project - Simulation

### Noun explaination:
* Port: 港口
* Tanker: 油輪，油輪於港口進行裝油
* Tug: 拖船，將船從harbor運至berth;將船從harbor運出berth
* Harbor: 港口
* berth: 船舶，船停放的地點
* berthing: 將船從港口運至船舶
* deberthing: 將船從船舶運出港口

### Rule
1. 1 tug at the port.
2. Tankers of all types require the services of a tug to move from harbor into a berth and later to out of a berth into the harbor.
3. It takes 0.25 hour to travel from the harbor to the berths, or vice versa (when it is not pulling a tanker).

* server capacity : 3 tankers
* tanker inter arrival time: Uniform(4,18)
* tanker type

| Type | Relative frequency | Loading time  | 
|-----------------|:-------------|:---------------:|
|  1   | 0.25  | Uniform(16,20)      |
|  2   | 0.25  | Uniform(21,27)      |
|  3   | 0.50  | Uniform(32,40)      |

> Tug rule:
```
berthing or deberthing takes 1 hour
travel takes 0.25 hour
When tug finish berthing activity --> 3 case:
    if deberth queue is not null:
        deberth the tanker in deberth queue
    elif harbor queue is not null && loading available:
        travel to harbor and berth the tanker in berth queue
    else stay in berth
When tug finish deberthing activity --> 2 case:
    if harbor queue is not null && loading available:
        berth the tanker in harbor queue
    elif deberth queue is not null
        travel to the berth and deberth the tanker in deberth queue
    else
        travel to the berth
```

> Tropical storm
```
storm length = uniform(2,6)
inter arrival = expon(48)
If the tug is traveling from the berths to the harbor without a tanker --> go back to berth
else if the tug is deberthing --> Finish existing activity



| ID    |   List        | Attribute 1                 | Attribute 2  | Attribute 3 |
|------ |:--------------|:----------------------------|:-------------|:---------------:|
|  1    | Harbor queue  | Time of arrival to queue    |              |                 |
|  2    | Deberth queue | Time of arrival to queue    |              |                 |
|  3    | Tug           | Time of arrival to queue    |              |                 |
|  4    | Loading server 1   | Time of arrival to server    |    | Record server ID |
|  5    | Loading server 2   | Time of arrival to server    |    | Record server ID |
|  6    | Loading server 3   | Time of arrival to server    |    | Record server ID |
|  7   | Tug_goback    |  Time to go back                  |    | Record leave time           |                  
|  25   | event list    | Event time                   | Event type   |            |                  
