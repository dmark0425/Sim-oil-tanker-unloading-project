// Define limits
#define MAX_LIST			25
#define MAX_ATTR			10
#define MAX_SVAR			25
#define TIM_VAR				25
#define MAX_TVAR			50
#define EPSILON				0.001
// Define array sizes
#define LIST_SIZE			26
#define ATTR_SIZE			11
#define SVAR_SIZE			26
#define TVAR_SIZE			51

// Define options for list_file and list_remoe.
#define FIRST 				 1
#define LAST  				 2
#define INCREASING     3
#define DECREASING     4

#define LIST_EVENT		25


#define EVENT_TIME		 1
#define EVENT_TYPE		 2

